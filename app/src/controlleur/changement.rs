
use std::thread;

use std::time::Duration;
use std::thread::JoinHandle;

use crate::modele::jeu::Jeu;
use crate::modele::creature::Creature;


pub struct Changement{
    jeu: Jeu
}

impl Changement{
    
    pub fn new(jeu : Jeu) -> Changement{
         Changement{
             jeu
            } 
        }

    pub fn start_changement(self) -> JoinHandle<()> {
        thread::spawn(|| {
            self.changement_loop()
        })
    }

    fn changement_loop(self){
        let creature_arc = self.jeu.get_creature_arc();
        let mut cmpt = 0;
        let duration = Duration::from_millis(1000);

        loop{
            cmpt = (cmpt + 1)%30;
            {
            let mut creature = creature_arc.lock().unwrap();

            let nb_faim = creature.get_faim();
            let nb_social = creature.get_social();
            let nb_sommeil = creature.get_sommeil();

            creature.blesser();

            if (cmpt == 0) | (cmpt == 10) | (cmpt == 20){
                creature.set_faim(nb_faim - 1);
            }

            if (cmpt == 14) | (cmpt == 29){
                creature.set_social(nb_social - 1);
            }

            if cmpt == 25{
                creature.set_sommeil(nb_sommeil + 1);
            }
            }
            thread::sleep(duration);
            
            

        }


    }



}