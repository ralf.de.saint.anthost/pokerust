use crate::modele::creature::Creature;
use std::sync::MutexGuard;

const NOURRIR: &str = "Vous nourrissez le creature";
const JOUER: &str = "Vous jouez avec le creature";
const ERROR: &str = "La commande n'est pas reconnue";


pub fn nourrir<T: Creature>(mut creature: MutexGuard<T>){
        let n = creature.get_faim();
        creature.set_faim(n+1);
        creature.set_derniere_action(NOURRIR);
        println!("{}",NOURRIR);
}

pub fn jouer<T: Creature>(mut creature: MutexGuard<T>){
        let n = creature.get_social();
        creature.set_social(n+1);

        let som = creature.get_sommeil();
        creature.set_sommeil(som+1);
        
        creature.set_derniere_action(JOUER);
        println!("{}",JOUER);
}


pub fn error<T: Creature>(mut creature: MutexGuard<T>){
        creature.set_derniere_action(ERROR);
        println!("{}",ERROR);
}
