mod vue;
mod modele;
mod controlleur;
use vue::{ihm::Ihm};
use crate::modele::jeu::Jeu;
use crate::controlleur::changement::Changement;

fn main() {
    println!("{} version {}",env!("CARGO_PKG_NAME"),env!("CARGO_PKG_VERSION"));
    let jeu: Jeu = Jeu::new();
    let ihm: Ihm = Ihm::new(jeu.clone());

    let changement: Changement = Changement::new(jeu.clone());

    let ihm_thread = ihm.start_ihm();
    changement.start_changement();

    ihm_thread.join().unwrap();
}
