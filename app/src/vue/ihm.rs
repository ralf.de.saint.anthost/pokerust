use std::{io, thread};
use std::thread::JoinHandle;
use crate::controlleur::action;
use std::sync::{Arc, Mutex};
extern crate piston_window;
extern crate find_folder;

use piston_window::*;
use crate::modele::dragon::Dragon;
use crate::modele::creature::Creature;
use crate::modele::jeu::Jeu;
use std::sync::MutexGuard;



/*
    Source de l'image du dragon :
    Created by ZaPaper & Jordan Irwin (AntumDeluge); Credits to http://www.buko-studios.com/ Commissioned by PlayCraft: https://www.playcraftapp.com/
 */

pub struct Ihm{
    jeu: Jeu
}

impl Ihm{
    pub fn new(jeu : Jeu) -> Ihm{ Ihm{jeu} }

    fn read() -> Option<String>{
        let mut entree = String::new();

        match io::stdin().read_line(&mut entree) { // on récupère ce qu'a entré l'utilisateur dans la variable entree
            Ok(_) => Some(entree),
            _ => { // une erreur s'est produite, on doit avertir l'utilisateur !
                println!("Erreur lors de la récupération de la saisie...");
                None
            }
        }
    }

    fn parse(dragon : MutexGuard<Dragon>, cmdline : &str){

        let cmd : &str = cmdline.trim_end();
        match cmd {
            "/Status" => {},
            "/Nourrir" => {action::nourrir(dragon)},
            "/Jouer" => {action::jouer(dragon)},
            _ => { action::error(dragon)}
        }
    }

    pub fn start_ihm(self) -> JoinHandle<()> {
        thread::spawn(|| {
            self.ihm_loop()
        })
    }

    fn ihm_loop(self)
    {
        let dragon_arc_read = self.jeu.get_creature_arc();
        let dragon_arc_window = self.jeu.get_creature_arc();
        thread::spawn(move|| {
            loop{
                let cmd = Ihm::read().unwrap();
                Ihm::parse(dragon_arc_read.lock().unwrap(),&cmd);
            }
        });
        Ihm::window_loop(dragon_arc_window,Ihm::init_window());
    }

    fn init_window() -> PistonWindow{
        let title = format!("{} {}",env!("CARGO_PKG_NAME"),env!("CARGO_PKG_VERSION"));
        let mut window: PistonWindow = WindowSettings::new(
            title,
            [600, 600]
        )
            .graphics_api(OpenGL::V3_2)
            .exit_on_esc(true)
            .build()
            .unwrap();

        window.set_lazy(false);
        window
    }

    fn window_loop(dragon_arc : Arc<Mutex<Dragon>>, mut window: PistonWindow)
    {
        let assets = find_folder::Search::ParentsThenKids(3, 3)
            .for_folder("assets").unwrap();
        let mut glyphs = window.load_font(assets.join("FiraSans-Regular.ttf")).unwrap();


        let dragon_u1 : G2dTexture = Texture::from_path(
            &mut window.create_texture_context(),
            assets.join("anim/dragonU1.png"),
            Flip::None,
            &TextureSettings::new()
        ).unwrap();
        let dragon_u2 : G2dTexture = Texture::from_path(
            &mut window.create_texture_context(),
            assets.join("anim/dragonU2.png"),
            Flip::None,
            &TextureSettings::new()
        ).unwrap();
        let dragon_u3 : G2dTexture = Texture::from_path(
            &mut window.create_texture_context(),
            assets.join("anim/dragonU3.png"),
            Flip::None,
            &TextureSettings::new()
        ).unwrap();

        let up = [dragon_u1,dragon_u2,dragon_u3];


        let mut anim:f64 = 0.0;
        let mut events = Events::new(EventSettings::new());
        while let Some(e) = events.next(&mut window) {
            if let Some(args) = e.update_args() {
                anim = (anim +4.0*args.dt)%3.0;
            }
            let dragon = dragon_arc.lock().unwrap();

            window.draw_2d(&e, |c, g, _device| {
                clear([1.0, 1.0, 1.0, 1.0], g);
                let center = c.transform.trans(8.0, 482.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,284.0,24.0);
                let red = [0.0, 0.0, 0.0, 0.1];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });
            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(8.0, 512.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,284.0,24.0);
                let red = [0.0, 0.0, 0.0, 0.1];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });
            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(308.0, 482.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,284.0,24.0);
                let red = [0.0, 0.0, 0.0, 0.1];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });
            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(308.0, 512.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,284.0 ,24.0);
                let red = [0.0, 0.0, 0.0, 0.1];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });

            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(10.0, 484.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,10.0 * (dragon.get_faim() as f64) ,20.0);
                let red = [1.0, 0.0, 0.0, 1.0];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });
            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(10.0, 514.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,10.0 * (dragon.get_social() as f64) ,20.0);
                let red = [1.0, 0.0, 0.0, 1.0];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });
            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(310.0, 484.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,10.0 * (dragon.get_sommeil() as f64) ,20.0);
                let red = [1.0, 0.0, 0.0, 1.0];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });
            window.draw_2d(&e, |c, g, _device| {
                let center = c.transform.trans(310.0, 514.0);
                let square = rectangle::rectangle_by_corners(0.0,0.0,10.0 * (dragon.get_vie() as f64) ,20.0);
                let red = [1.0, 0.0, 0.0, 1.0];
                rectangle(red, square, center, g); // We translate the rectangle slightly so that it's centered; otherwise only the top left corner would be centered
            });


            window.draw_2d(&e, |c, g, device| {
                text::Text::new(20).draw(
                    format!("Faim : {}",
                            dragon.get_faim()).as_str(),
                    &mut glyphs,
                    &c.draw_state,
                    c.transform.trans(10.0, 500.0),
                    g
                ).unwrap();
                glyphs.factory.encoder.flush(device);
            });
            window.draw_2d(&e, |c, g, device| {
                text::Text::new(20).draw(
                    format!("Social : {} ",
                            dragon.get_social()).as_str(),
                    &mut glyphs,
                    &c.draw_state,
                    c.transform.trans(10.0, 530.0),
                    g
                ).unwrap();
                glyphs.factory.encoder.flush(device);
            });
            window.draw_2d(&e, |c, g, device| {
                text::Text::new(20).draw(
                    format!("Sommeil : {}",
                            dragon.get_sommeil()).as_str(),
                    &mut glyphs,
                    &c.draw_state,
                    c.transform.trans(310.0, 500.0),
                    g
                ).unwrap();
                glyphs.factory.encoder.flush(device);
            });
            window.draw_2d(&e, |c, g, device| {
                text::Text::new(20).draw(
                    format!("Vie : {}",
                            dragon.get_vie()).as_str(),
                    &mut glyphs,
                    &c.draw_state,
                    c.transform.trans(310.0, 530.0),
                    g
                ).unwrap();
                glyphs.factory.encoder.flush(device);
            });


            window.draw_2d(&e, |c, g, device| {
                text::Text::new(20).draw(
                    format!("{}",dragon.get_derniere_action()).as_str(),
                    &mut glyphs,
                    &c.draw_state,
                    c.transform.trans(10.0, 570.0),
                    g
                ).unwrap();
                glyphs.factory.encoder.flush(device);
            });
            window.draw_2d(&e,|c, g, _|
                {
                    image(&up[anim.floor() as usize],c.transform.trans(200.0, 200.0), g);
                });
        }
    }
}