use crate::modele::creature::Creature;


pub struct Dragon {
    faim: i32,
    social: i32,
    sommeil: i32,
    vie: i32,
    derniere_action: String
}

impl Creature for Dragon{

    fn new() -> Dragon {
        Dragon{
            faim : 28, 
            social : 28,
            sommeil : 0,
            vie : 28,
            derniere_action : "".parse().unwrap()
        }
    }

    fn set_faim(&mut self, faim : i32){
        if faim > 28{
            self.faim = 28
        }else if faim < 0{
            self.faim = 0
        }else{
            self.faim = faim;
        }
    }


    fn set_social(&mut self, social: i32){
        if social > 28{
            self.social = 28
        }else if social < 0{
            self.social = 0
        }else{
            self.social = social;
        }
    }

    fn set_sommeil(&mut self, sommeil: i32){
        if sommeil > 28{
            self.sommeil = 28
        }else if sommeil < 0{
            self.sommeil = 0
        }else{
            self.sommeil = sommeil;
        }
    }

    fn set_vie(&mut self, vie: i32){
        if vie > 28{
            self.vie = 28
        }else if vie < 0{
            self.vie = 0
        }else{
            self.vie = vie;
        }
    }

    fn set_derniere_action(&mut self, derniere_action : &str){
        self.derniere_action = derniere_action.parse().unwrap();
    }

    fn get_faim(&self) -> i32{
        self.faim
    }

    fn get_social(&self) -> i32{
        self.social
    }

    fn get_sommeil(&self) -> i32{
        self.sommeil
    }

    fn get_vie(&self) -> i32{
        self.vie
    }

    fn get_derniere_action(&self) -> &str {
        self.derniere_action.as_str()
    }

    fn blesser(&mut self){
        let n = self.get_vie();

        if (self.get_faim() == 0) || (self.get_social() == 0) || (self.get_sommeil() == 28){
                self.set_vie(n-1);
        }else if (self.get_faim() > 25) && (self.get_social() != 0) && (self.get_sommeil() != 28){
                self.set_vie(n+1);
        }else if  (self.get_faim() != 0) && (self.get_social() > 25) && (self.get_sommeil() !=28){
                self.set_vie(n+1);
        }else if  (self.get_faim() != 0) && (self.get_social() != 0) && (self.get_sommeil() < 5){
                self.set_vie(n+1);
        }

    }
}
