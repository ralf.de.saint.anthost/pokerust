use crate::modele::dragon::Dragon;
use std::sync::{Arc, Mutex};
use crate::modele::creature::Creature;


#[derive (Clone)]
pub struct Jeu{
    creature : Arc<Mutex<Dragon>>,
}

impl Jeu{
    pub fn new() -> Jeu{
        let creature = Arc::new(Mutex::new(Dragon::new()));
        Jeu{
            creature
        }
    }

    pub fn get_creature_arc(&self) -> Arc<Mutex<Dragon>>{
        self.creature.clone()
    }
}