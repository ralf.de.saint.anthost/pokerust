pub trait Creature {

    fn new() -> Self where Self: Sized;

    fn set_faim(&mut self, faim : i32);

    fn set_social(&mut self, social: i32);

    fn set_sommeil(&mut self, sommeil: i32);

    fn set_vie(&mut self, vie: i32);

    fn set_derniere_action(&mut self, derniere_action : &str);

    fn get_faim(&self) -> i32;

    fn get_social(&self) -> i32;

    fn get_sommeil(&self) -> i32;

    fn get_vie(&self) -> i32;

    fn get_derniere_action(&self) -> &str;

    fn blesser(&mut self);
}